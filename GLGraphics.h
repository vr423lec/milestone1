#define GLFW_INCLUDE_ES2
#define GLEW_STATIC

#include <GL/glew.h>
#include <GLFW/glfw3.h>
#include <SOIL.h>
#include <glm.hpp>
#include <gtc\matrix_transform.hpp>
#include <gtc\type_ptr.hpp>

class GLGraphics
{
public:
	GLGraphics(GLFWwindow* win);
	~GLGraphics();
private:
	GLFWwindow* window;
public:
	void BeginFrame();
	void EndFrame();
	void Init();
};