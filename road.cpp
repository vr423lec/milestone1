#include "road.h"


void road::GenRoad(Shader sh, point p1, point p2, point p3, point p4, float index)
{

	glBindVertexArray(VAO);

	float coord[] = {
		p1.x, p1.y, p1.z, 0.0f, 0.0f,
		p2.x, p2.y, p2.z, 1.0f, 0.0f,
		p3.x, p3.y, p3.z, 1.0f, 1.0f,
		p4.x, p4.y, p4.z, 0.0f, 1.0f
	};
	glBindTexture(GL_TEXTURE_2D, RoadTexture.getID());
	
	vertexObj.add_data(coord, sizeof(coord));
	vertexObj.uploadData(GL_STATIC_DRAW);

	GLuint elem[] = {
		0, 1, 2,
		2, 3, 0
	};

	EBO.add_data(elem, sizeof(elem));
	EBO.uploadData(GL_STATIC_DRAW);

	glm::mat4 model;
	model = glm::translate(model, glm::vec3(0.0f, -2.0f, 0.0f));
	model = glm::rotate(model, glm::radians(90.f), glm::vec3(1.0f, 0.0f, 0.0f));
	model = glm::rotate(model, glm::radians(90.f), glm::vec3(0.0f, 0.0f, 1.0f));
	model = glm::scale(model, glm::vec3(20.0f,20.0f, 15.0f));
	model = glm::translate(model, glm::vec3(0.0f, -index, 0.0f));
	glUniformMatrix4fv(glGetUniformLocation(sh.Program, "model"), 1, GL_FALSE, glm::value_ptr(model));
	glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_INT, 0);
	glBindVertexArray(0);
}

void road::setup(Shader sh)
{
	glGenVertexArrays(1, &VAO);

	glBindVertexArray(VAO);
	vertexObj.createVBO();

	vertexObj.bindVBO(GL_ARRAY_BUFFER);
	EBO.createVBO();
	EBO.bindVBO(GL_ELEMENT_ARRAY_BUFFER);

	RoadTexture.LoadTexture2D("C:/Users/florentina.liscan/Documents/bit/road.jpg", true);
	GLint pos = glGetAttribLocation(sh.Program, "position");
	glEnableVertexAttribArray(0);
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 5 * sizeof(float), nullptr);
	GLint tex = glGetAttribLocation(sh.Program, "texCoords");
	glEnableVertexAttribArray(2);
	glVertexAttribPointer(2, 2, GL_FLOAT, GL_FALSE, 5 * sizeof(float), (void*)(3 * sizeof(float)));
	glBindVertexArray(0);
}



void road::Render(Shader shader,float index)
{
		/*for (float i = -6.0f; i <= 5.0f; i = i + 1.0f)
		{
			point p1, p2, p3, p4;
			p1.x = -1.0f;
			p1.y = 1.0f;
			p2.x = 0.3f;
			p2.y = 1.0f;
			p3.x = 0.3f;
			p3.y = -1.0f;
			p4.x = -1.0f;
			p4.y = -1.0f;
			GenRoad(shader, p1, p2, p3, p4, i + x);
			p1.x = -0.3f;
			p1.y = 1.0f;
			p2.x = 1.0f;
			p2.y = 1.0f;
			p3.x = 1.0f;
			p3.y = -1.0f;
			p4.x = -0.3f;
			p4.y = -1.0f;
			GenRoad(shader, p1, p2, p3, p4, i + x);
			if ((index > z + 60.0f) && (i == 5.0f))
			{
				y = x + index/2.0f/10.0f + 3.0f;
				z = z + 60.0f;
			}
		}*/
		for (float i = -6.0f; i <= 5.0f; i = i + 1.0f)
		{
			point p1, p2, p3, p4;
			p1.x = -1.0f;
			p1.y = 1.0f;
			p2.x = 0.3f;
			p2.y = 1.0f;
			p3.x = 0.3f;
			p3.y = -1.0f;
			p4.x = -1.0f;
			p4.y = -1.0f;
			GenRoad(shader, p1, p2, p3, p4, i + y);
			p1.x = -0.3f;
			p1.y = 1.0f;
			p2.x = 1.0f;
			p2.y = 1.0f;
			p3.x = 1.0f;
			p3.y = -1.0f;
			p4.x = -0.3f;
			p4.y = -1.0f;
			GenRoad(shader, p1, p2, p3, p4, i + y);
			if ((index > z + 60.0f) && (i == 5.0f))
			{
				y = index/2.0f/10.0f + 3.0f;
				z = z + 60.0f;
			}
		}
}

road::road() 
{
	x = 0.0f;
	y = 0.0f;
	z = 0.0f;
	a = 1.0f;
}

road::~road()
{
	EBO.deleteVBO();
	vertexObj.deleteVBO();
	glDeleteVertexArrays(1, &VAO);
}
