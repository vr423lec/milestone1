#pragma once
#include "Header.h"


class TextureHandler
{
public:
	TextureHandler();
	void CreateEmptyTexture(int width, int height, GLenum format);
	void CreateFromData(BYTE* data, int width, int height, int BPP, GLenum format, bool generateMipMaps = false);
	bool LoadTexture2D(std::string path, bool generateMipMaps = false);
	void BindTexture(int iTextureUnit = 0);
	std::string getPath();
	UINT getID();

private:
	GLuint texture;
	int width, height, BPP;
	bool mmGenerated;
	std::string path;
};
