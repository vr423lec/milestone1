#include "Simulator.h"
#include <stdlib.h>
#include <time.h>

Simulator::Simulator(GLFWwindow* win) : window(win), gfx(win)
{
	gfx.Init();
	shader.create("C:/Users/florentina.liscan/Documents/bit/Shaders/model_loading.vert", "C:/Users/florentina.liscan/Documents/bit/Shaders/model_loading.frag");
	ourVehicle = new Vehicle();
	ourVehicle->loadModel();
	randomVar = 0.0f;
	trSwitchLane = 0.0f;
	ourSpeed = 60.0f;
	trSpeed = 0.0f;
	ourRoad.setup(shader);
	ourVehicle->set_position(18.0f, -12.0f);
	Vehicle* veh1, *veh2;
	veh1 = new Vehicle();
	veh2 = new Vehicle();
	veh2->set_position(-2.0f, 12.0f);
	veh1->set_position(-25.0f, -12.0f);
	traffic.push_back(ourVehicle);
	traffic.push_back(veh1);
	traffic.push_back(veh2);
	srand(std::time(NULL));
	rnd = 0;
	test = false;
	randVar = 1;
}

Simulator::~Simulator()
{
} 

void Simulator::Go(float deltatime)
{
	gfx.BeginFrame();
	CreateFrame();
//	updatePositions();
	manageTraffic();
	for (int pos = 1; pos < traffic.size();pos++)
	{
		if (ourVehicle->getPositionY() == traffic[pos]->getPositionY() && ourVehicle->getPositionX() < traffic[pos]->getPositionX() && (ourVehicle->getTargetSpeed() > traffic[pos]->getTargetSpeed() || ourVehicle->getSpeed() > traffic[pos]->getSpeed()))
		{
			test = true;
		}
	}
	if (test == true)
	{
		if (randVar == 1)
		{
			if (checkIfSafeC(*ourVehicle) == true) ourVehicle->change_lane_center(shader, deltatime);
			else if (checkIfSafeR(*ourVehicle) == true) ourVehicle->change_lane_right(shader, deltatime);
		}
		else if (randVar == 2)
		{
			if (checkIfSafeL(*ourVehicle) == true) ourVehicle->change_lane_left(shader, deltatime);
			else if (checkIfSafeR(*ourVehicle) == true) ourVehicle->change_lane_right(shader, deltatime);
		}
		else if (randVar == 3)
		{
			if (checkIfSafeC(*ourVehicle) == true) ourVehicle->change_lane_center(shader, deltatime);
			else if (checkIfSafeL(*ourVehicle) == true) ourVehicle->change_lane_left(shader, deltatime);
		}
	}
	ourVehicle->motion(deltatime);

	if (randomVar > 9950)
	{
		bool chSpeed = true;
		for (int i = 0; i < traffic.size(); i++)
		{
			if (ourVehicle->getPositionY() == traffic[i]->getPositionY() && ourVehicle->getPositionX() < traffic[i]->getPositionX())
				chSpeed = false;
		}
		if(chSpeed == true && ourVehicle->getTargetSpeed() == ourVehicle->getSpeed()) ourVehicle->set_targetSpeed(rand() % 120 + 60.0f);
	}
	if (randomVar > 8000 && randomVar < 8100)
	{
		trSpeed = rand() % (traffic.size() - 1) + 1;
		bool chSpeed = true;
		for (int i = 1; i < traffic.size(); i++)
		{
			if ((traffic[trSpeed]->getPositionY() == traffic[i]->getPositionY() && traffic[trSpeed]->getPositionX() < traffic[i]->getPositionX()))
				chSpeed = false;
		}
		if (chSpeed == true) traffic[trSpeed]->set_targetSpeed(rand() % 120 + 60.0f);
	}
	for (int i = 1; i < traffic.size(); i++)
	{
		for (int pos = 0; pos < traffic.size();pos++)
		{
			if (abs(traffic[i]->getPositionY() - traffic[pos]->getPositionY()) < 12.0f && traffic[i]->getPositionX() < traffic[pos]->getPositionX() && (traffic[i]->getTargetSpeed() > traffic[pos]->getTargetSpeed() || traffic[i]->getSpeed() > traffic[pos]->getSpeed()))
			{
				if (traffic[i]->change == 1)
				{
					if (checkIfSafeC(*traffic[i]) == true) traffic[i]->change_lane_center(shader, deltatime);
					else if (checkIfSafeR(*traffic[i]) == true) traffic[i]->change_lane_right(shader, deltatime);
				}
				else if (traffic[i]->change == 2)
				{
					if (checkIfSafeL(*traffic[i]) == true) traffic[i]->change_lane_left(shader, deltatime);
					else if (checkIfSafeR(*traffic[i]) == true) traffic[i]->change_lane_right(shader, deltatime);
				}
				else if (traffic[i]->change == 3)
				{
					if (checkIfSafeC(*traffic[i]) == true) traffic[i]->change_lane_center(shader, deltatime);
					else if (checkIfSafeL(*traffic[i]) == true) traffic[i]->change_lane_left(shader, deltatime);
				}
			}
		}
		if (traffic[i]->getPositionY() == 12.0f || traffic[i]->getPositionY() == 0.0f || traffic[i]->getPositionY() == -12.0f)
			traffic[i]->change = rand() % 3 + 1;
		traffic[i]->motion(deltatime);
	}
	if (ourVehicle->getPositionY() == 12.0f || ourVehicle->getPositionY() == 0.0f || ourVehicle->getPositionY() == -12.0f)
	{
		test = false;
		int index = rand() % 10000 + 1;
		randVar = rand() % 3 + 1;
		randomVar = index;
	}
	/*traffic_control(deltatime);
	if (randomVar > 1 && randomVar < 5)
	{
		if (checkIfSafeC(ourVehicle) == true) ourVehicle->change_lane_center(shader, deltatime * 2);
	}
	if (randomVar > 51 && randomVar < 55)
	{
		if ((checkIfSafeL(ourVehicle) == true)) ourVehicle->change_lane_left(shader, deltatime * 2);
	}

	if (randomVar > 100 && randomVar < 105)
	{
		if((checkIfSafeR(ourVehicle) == true)) ourVehicle->change_lane_right(shader, deltatime * 2);
	}*/
	gfx.EndFrame();
}

void Simulator::CreateFrame()
{
	shader.Use();
	glm::mat4 projection = glm::perspective(glm::radians(90.0f), (float)800.0f / (float)600.0f, 0.1f, 100.0f);
	glm::mat4 view = glm::lookAt(glm::vec3(ourVehicle->getPositionX(), 40.0f, 0.0f), glm::vec3(ourVehicle->getPositionX(), 0.0f, 0.0f), glm::vec3(0.0f, 0.0f, 1.0f));
	glUniformMatrix4fv(glGetUniformLocation(shader.Program, "projection"), 1, GL_FALSE, glm::value_ptr(projection));
	glUniformMatrix4fv(glGetUniformLocation(shader.Program, "view"), 1, GL_FALSE, glm::value_ptr(view));
	ourVehicle->Render(shader);
	ourRoad.Render(shader, ourVehicle->getPositionX());
	for (int i = 1; i < traffic.size(); i++)
	{
		traffic[i]->Render(shader);
	}
}

void Simulator::traffic_control(float deltatime)
{
	if (traffic[rnd]->getPositionY() == 12.0f || traffic[rnd]->getPositionY() == 0.0f || traffic[rnd]->getPositionY() == -12.0f)
	{
		rnd = rand() % (traffic.size() - 1) + 1;
		trSwitchLane = rand() % 10000 + 1;
	}
	
			if (trSwitchLane == 1)
			{
				if (checkIfSafeC(*traffic[rnd]) == true) traffic[rnd]->change_lane_center(shader, deltatime * 2);
			}
			if (trSwitchLane == 2)
			{
				if (checkIfSafeR(*traffic[rnd]) == true) traffic[rnd]->change_lane_right(shader, deltatime * 2);
			}
			if (trSwitchLane == 3)
			{
				if (checkIfSafeL(*traffic[rnd]) == true) traffic[rnd]->change_lane_left(shader, deltatime * 2);
			}
}

/*void Simulator::updatePositions()
{
	positions.clear();
	position pos;
	pos.pX = ourVehicle->getPositionX();
	pos.pY = ourVehicle->getPositionY();
	positions.push_back(pos);
	for (int i = 1; i < traffic.size(); i++)
	{
		position pos;
		pos.pX = traffic[i].getPositionX();
		pos.pY = traffic[i].getPositionY();
		positions.push_back(pos);
	}
}*/

void Simulator::manageTraffic()
{
	for (int i = 1;i < traffic.size();i++)
	{
		float poses[] = { -12.0f,0.0f,12.0f };
		float y = poses[rand() % 3];
		if (traffic[i]->getPositionX() > ourVehicle->getPositionX() + 100.0f)
		{
		//	positions.erase(positions.begin() + i);
		//	traffic[i]->reset();
			traffic.erase(traffic.begin() + i);
			Vehicle* veh = new Vehicle();
			veh->set_position(ourVehicle->getPositionX() - 100.0f, y);
			veh->set_speed(ourVehicle->getTargetSpeed() + 20.0f);
			traffic.push_back(veh);
			position pos;
			pos.pX = veh->getPositionX();
			pos.pY = veh->getPositionY();
			positions.push_back(pos);
		}
		if (traffic[i]->getPositionX() < ourVehicle->getPositionX() - 100.0f)
		{
		//	positions.erase(positions.begin() + i);
		//	traffic[i]->reset();
			traffic.erase(traffic.begin() + i);
			Vehicle* veh = new Vehicle();
			veh->set_position(ourVehicle->getPositionX() + 100.0f, y);
			veh->set_speed(ourVehicle->getTargetSpeed() - 20.0f);
			traffic.push_back(veh);
			position pos;
			pos.pX = veh->getPositionX();
			pos.pY = veh->getPositionY();
			positions.push_back(pos);
		}
	}
}

bool Simulator::checkIfSafeC(Vehicle trVeh)
{
		for (int pos = 0; pos < traffic.size(); pos++)
			if (!(abs(trVeh.getPositionX() - traffic[pos]->getPositionX()) > 12.0f) && traffic[pos]->getPositionY() == 0.0f)
				return false;
	return true;
}

bool Simulator::checkIfSafeR(Vehicle trVeh)
{
		for (int pos = 0; pos < traffic.size(); pos++)
			if (!(abs(trVeh.getPositionX() - traffic[pos]->getPositionX()) > 12.0f) && (traffic[pos]->getPositionY() == 12.0f || trVeh.getPositionY() + 12.0f == traffic[pos]->getPositionY()))
				return false;
	return true;
}

bool Simulator::checkIfSafeL(Vehicle trVeh)
{
		for (int pos = 0; pos < traffic.size(); pos++)
			if (!(abs(trVeh.getPositionX() - traffic[pos]->getPositionX()) > 12.0f) && (traffic[pos]->getPositionY() == -12.0f || trVeh.getPositionY() - 12.0f == traffic[pos]->getPositionY()))
				return false;
	return true;
}