#include "vertexBufferObj.h"

vertexBufferObj::vertexBufferObj(int size)
{
	/*glGenBuffers(1, &buffer);
	data.reserve(size);
	current_size = 0;*/
}

void vertexBufferObj::createVBO(int size)
{
	glGenBuffers(1, &buffer);
	data.reserve(size);
	current_size = 0;
}

void vertexBufferObj::deleteVBO()
{
	glDeleteBuffers(1, &buffer);
	data.clear();
}

void vertexBufferObj::bindVBO(int type)
{
	bufferType = type;
	glBindBuffer(bufferType, buffer);
}

void vertexBufferObj::uploadData(int drawMode)
{
	glBufferData(bufferType, data.size(), &data[0], drawMode);
	data.clear();
}

void vertexBufferObj::add_data(void* ptr, unsigned int dataSize)
{
	data.insert(data.end(), (BYTE*)ptr, (BYTE*)ptr + dataSize);
	current_size += dataSize;
}

int vertexBufferObj::getCurrentSize()
{
	return current_size;
}

int vertexBufferObj::getID()
{
	return buffer;
}