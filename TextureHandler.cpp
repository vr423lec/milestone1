#include "TextureHandler.h";
#include <SOIL.h>

TextureHandler::TextureHandler()
{
	texture = 0;
	mmGenerated = false;
}

void TextureHandler::CreateEmptyTexture(int width, int height, GLenum format)
{
	glGenTextures(1, &texture);
	glBindTexture(GL_TEXTURE_2D, texture);
	if (format == GL_RGBA || format == GL_BGRA)
		glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, width, height, 0, format, GL_UNSIGNED_BYTE, NULL);
	else if (format == GL_RGB || format == GL_BGR)
		glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, width, height, 0, format, GL_UNSIGNED_BYTE, NULL);
	else
		glTexImage2D(GL_TEXTURE_2D, 0, format, width, height, 0, format, GL_UNSIGNED_BYTE, NULL);
}

void TextureHandler::CreateFromData(BYTE* data, int width, int height, int BPP, GLenum format, bool generateMipMaps)
{
	glGenTextures(1, &texture);
	glBindTexture(GL_TEXTURE_2D, texture);
	if (format == GL_RGBA || format == GL_BGRA)
		glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, width, height, 0, format, GL_UNSIGNED_BYTE, data);
	else if (format == GL_RGB || format == GL_BGR)
		glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, width, height, 0, format, GL_UNSIGNED_BYTE, data);
	else
		glTexImage2D(GL_TEXTURE_2D, 0, format, width, height, 0, format, GL_UNSIGNED_BYTE, data);
	if (generateMipMaps)glGenerateMipmap(GL_TEXTURE_2D);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glBindTexture(GL_TEXTURE_2D, 0);
	path = "";
	mmGenerated = generateMipMaps;
	this->width = width;
	this->height = height;
	this->BPP = BPP;
}

bool TextureHandler::LoadTexture2D(std::string path, bool generateMipMaps)
{
	BYTE* data = SOIL_load_image(path.c_str(), &width, &height, 0, SOIL_LOAD_RGBA);
	CreateFromData(data, width, height, 32, GL_RGBA, generateMipMaps);
	SOIL_free_image_data(data);
	this->path = path;
	return true;
}

void TextureHandler::BindTexture(int tunit)
{
	glActiveTexture(GL_TEXTURE0 + tunit);
	glBindTexture(GL_TEXTURE_2D, texture);
}

std::string TextureHandler::getPath()
{
	return path;
}


UINT TextureHandler::getID()
{
	return texture;
}