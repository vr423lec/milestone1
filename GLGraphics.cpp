#include "GLGraphics.h"


GLGraphics::GLGraphics(GLFWwindow* win)
{
	window = win;
}

GLGraphics::~GLGraphics()
{
}

void GLGraphics::BeginFrame()
{
	glClearColor(0.0f, 1.0f, 0.0f, 1.0f);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
}

void GLGraphics::EndFrame()
{
	glfwSwapBuffers(window);
}

void GLGraphics::Init()
{

	glewExperimental = GL_TRUE;
	glewInit();
	glViewport(0, 0, 800, 600);
	glEnable(GL_DEPTH_TEST);

	
}