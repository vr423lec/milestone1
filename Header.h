#include <ctime>
#include <windows.h>

#include <algorithm>
#include <cmath>
#include <cstring>
#include <vector>
#include <sstream>
#include <queue>
#include <map>
#include <set>
#include <gl/glew.h>
#include <glm.hpp>

#include <gtc\matrix_transform.hpp>
#include <gtc\type_ptr.hpp>