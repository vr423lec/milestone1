#pragma once
#include "GLGraphics.h"
#include "Vehicle.h"
#include "road.h"

struct position
{
	float pX, pY;
};

class Simulator
{
public:
	Simulator(GLFWwindow* win);
	~Simulator();
	void Go(float deltatime);

private:
	void CreateFrame();
	void traffic_control(float deltatime);
	void updatePositions();
	void manageTraffic();

	bool checkIfSafeL(Vehicle trVeh);
	bool checkIfSafeR(Vehicle trVeh);
	bool checkIfSafeC(Vehicle trVeh);
private:
	GLFWwindow* window;
	GLGraphics gfx;
	Shader shader;
	Vehicle* ourVehicle;
	vector <Vehicle*> traffic;
	vector <position> positions;
	float randomVar , trSwitchLane, randVar;
	float ourSpeed, trSpeed;
	road ourRoad;
	int rnd;
	bool test;
};
